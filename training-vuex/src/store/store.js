import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import getters from './getters'
import mutations from './mutations'
import actions from './actions'

export default new Vuex.Store({
    state: {
        listTodos: [
            {id: 1, title: 'To do 1'},
            {id: 2, title: 'To do 2'},
            {id: 3, title: 'To do 3'},
            {id: 4, title: 'To do 4'},
            {id: 5, title: 'To do 5'},
            {id: 6, title: 'To do 6'},
            {id: 7, title: 'To do 7'},
        ],
    },
    getters,
    mutations,
    actions
})