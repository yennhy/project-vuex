import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

// export const store = new Vuex.Store({
// 	state: {
// 		listTodos: [
// 			{ id: 1, title: 'viec 1', completed: false },
// 			{ id: 2, title: 'viec 2', completed: false },
// 			{ id: 3, title: 'viec 3', completed: false },
// 			{ id: 4, title: 'viec 4', completed: false },
// 			{ id: 5, title: 'viec 5', completed: false },
// 		]
// 	},
// })

// export const store = new Vuex.Store({
// 	state: {
// 		count: 0
// 	},
// 	mutations: {
// 		increment (state) {
// 			state.count++
// 		}
// 	}
// })

export const store = new Vuex.Store({
	state: {
		safelyStoredNumber: 0
	},
	getters: {
		safelyStoredNumber: state => state.safelyStoredNumber,
		storedNumberMatches(state) {
		return matchNumber => {
			return state.safelyStoredNumber === matchNumber;
		}
	}
	  // Cách ngắn gọn hơn:
	  // storedNumberMatches: state => matchNumber => state.safelyStoredNumbers === matchNumber
	}
});